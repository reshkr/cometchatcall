import {axios, multiPartAxios,axiosCometChat} from "../Utils/config";
class AuthApiFetch {
    sendOtp(payload:any) {
        const reponse = axios.post(
            "send-otp", payload
        )         
        return reponse;
    }
    verifyOtp(payload:any) {
        const reponse = axios.post(
            "verify-otp", payload
        )         
        return reponse;
    }
}
export default new AuthApiFetch;