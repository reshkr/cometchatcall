import React, { useEffect, useState } from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  TextInput
} from 'react-native';
import { CometChat } from '@cometchat-pro/react-native-chat';
import AsyncStorage from "@react-native-async-storage/async-storage";
import { CometChatUIKit } from '@cometchat/chat-uikit-react-native'
import Api from '../../Api';

interface CometChatOutgoingCallProps {
    route:any;
    navigation:any;
}

const HomeScreen: React.FC<CometChatOutgoingCallProps> = ({route,navigation}) => {

    const [uid,setUid] = useState()

    useEffect(() => {
        comechatLogin()
    }, [])

    const comechatLogin = async () =>{
        try{
        const uids = await AsyncStorage.getItem('currentUserUid');  
        setUid(uids);
        CometChatUIKit.login({uid:uids})
        .then(user => {
                  console.log("User logged in successfully  \(user.getName())",user.getName())
        })
        .catch((error) => {
                  console.log("Login failed with exception:", error)
         })
        } catch (error) { 
          console.error('Error subscribing to topics:', error);
        }
      }  

    const callInitial = () => {
        
        var receiverID: string = uid === '8b733675-336c-461a-bed2-06f71ebd4aaf'?'c1e3cf8c-365a-48ad-a349-0d73c847c81d':'8b733675-336c-461a-bed2-06f71ebd4aaf';
        var callType: string = CometChat.CALL_TYPE.AUDIO;
        var receiverType: string = CometChat.RECEIVER_TYPE.USER;
        var call: CometChat.Call = new CometChat.Call(receiverID,callType,receiverType);

      CometChat.initiateCall(call).then(
        (outGoingCall: CometChat.Call) => {
          console.log("Call initiated successfully:", outGoingCall);
          navigation.navigate('CallScreen', { outGoingCall: outGoingCall, listnerId: `${uid === '8b733675-336c-461a-bed2-06f71ebd4aaf'?'c1e3cf8c-365a-48ad-a349-0d73c847c81d':'8b733675-336c-461a-bed2-06f71ebd4aaf'}_user_${uid}` })
        }, (error: CometChat.CometChatException) => {
          console.log("Call initialization failed with exception:", error);
          CometChat.getActiveCall()
          var sessionID = "v1.in.245904647869399c.1703583641c92906fdee6bc4eb6b97b96ca2fdd26f69dfb988";

        CometChat.endCall(sessionID).then(
          call => {
            console.log("Call rejected successfully", call);
          },
          error => {
            console.log("Call rejection failed with error:", error);
          }
        );

        }
      );
    }
  
    useEffect(() => {
      var listnerID = `${uid === '8b733675-336c-461a-bed2-06f71ebd4aaf'?'c1e3cf8c-365a-48ad-a349-0d73c847c81d':'8b733675-336c-461a-bed2-06f71ebd4aaf'}_user_${uid}`;
      try {
      CometChat.addCallListener(
        listnerID,
        new CometChat.CallListener({
          onIncomingCallReceived: (call: any) => {
            console.log("Incoming call:", call);
            navigation.navigate('IncomingCall', { outGoingCall: call , listnerId: `${uid === '8b733675-336c-461a-bed2-06f71ebd4aaf'?'c1e3cf8c-365a-48ad-a349-0d73c847c81d':'8b733675-336c-461a-bed2-06f71ebd4aaf'}_user_${uid}`})
            // Handle incoming call
          },
          onIncomingCallCancelled: (call: any) => {
            console.log("Incoming call calcelled:", call);
          },
        })
      );
    } catch (error) {
      console.error("Error adding call listener:", error);
      // Handle the error as needed
    }
      return () => {
        try {
          console.log("Before callListner");
          CometChat.removeCallListener(listnerID);
          console.log('listener deactivated with id call', listnerID);
        } catch (error) {
          console.error("Error call listener:", error);
          // Handle the error as needed
        }
      };
    }, []);

    const onPressLogOut = ()=>{
        CometChat.logout().then(async() => {
          await AsyncStorage.removeItem('userAccessToken');
          await AsyncStorage.removeItem('currentUserUid');
          navigation.reset({
            index: 0,
            routes: [{ name: 'LoginScreen' }],
          })
          console.log('Logout completed successfully');
        }, (error) => {
          console.log('Logout failed with exception:', { error });
        });
      }

    return (
     
        <View style={{ height: '100%', width: '100%', position: 'relative' ,alignItems:'center',justifyContent:'center'}}>
            <TouchableOpacity 
            style={{width:100,height:50,backgroundColor:'blue',alignItems:'center',justifyContent:'center'}}
            onPress={()=>callInitial()}>
                <Text style={{fontSize:20,color:'white'}}>Call</Text>
            </TouchableOpacity>
            <TouchableOpacity 
            style={{width:100,height:50,backgroundColor:'blue',alignItems:'center',justifyContent:'center',marginTop:30}}
            onPress={()=>onPressLogOut()}>
                <Text style={{fontSize:20,color:'white'}}>Logout</Text>
            </TouchableOpacity>
        </View>
     
    );
 
}
export default HomeScreen;
