import React, { useEffect, useState } from 'react';
import {
  Text,
  View,
  Modal,
  TouchableOpacity,
  Vibration,
  StyleSheet,
} from 'react-native';
import { CometChat } from '@cometchat-pro/react-native-chat';
import Icon from 'react-native-vector-icons/MaterialIcons';
import CometChatAvatar from './CometChatAvatar';
import { CometChatCalls } from "@cometchat-pro/react-native-calls";
import {CometChatOngoingCall} from '@cometchat/chat-uikit-react-native'


interface CometChatOutgoingCallProps {
    route:any;
    navigation:any;
    callback: (event: string, call: any) => void;
}

const CometChatOutgoingCall: React.FC<CometChatOutgoingCallProps> = ({route,navigation}) => {

  const [callSettings, setCallSettings] = useState<boolean>(false);
  const [errorScreen, setErrorScreen] = useState<boolean>(false);
  const [errorMessage, setErrorMessage] = useState<string | null>(null);
  const [callInProgress, setCallInProgress] = useState<any>(null);
  const [outgoingCallScreen, setOutgoingCallScreen] = useState<boolean>(false);

  useEffect(() => {
var listnerID = route?.params?.listnerId;
CometChat.addCallListener(
  listnerID,
  new CometChat.CallListener({
    onIncomingCallReceived: (call:any) => {
      console.log("Incoming call:", call);
      // Handle incoming call
    },
    onOutgoingCallAccepted: (call:any) => {
      console.log("Outgoing call accepted:", call);
      // startCall(call)
      // Outgoing Call Accepted
      setCallSettings(true)
    },
    onOutgoingCallRejected: (call:any) => {
      console.log("Outgoing call rejected:", call);
      navigation.goBack()
      // Outgoing Call Rejected
    },
    onIncomingCallCancelled: (call:any) => {
      console.log("Incoming call calcelled:", call);
      navigation.goBack()
    },
     onCallEndedMessageReceived: (call:any) => {
      console.log("CallEnded Message:", call);
    }
  })
);
}, []);

  // const startCall = (call:any) => {
  //   try {
  //       const { sessionId } = call;
  //       const callType = call.type;
  //       const audioOnly = callType === 'audio';
  
  //       const callListener = new CometChat.OngoingCallListener({
  //         onUserJoined: (user:any) => {
           
  //         },
  //         onUserLeft: (user:any) => {
           
  //         },
  //         onCallEnded: (endedCall:any) => {
  //           // this.setState({
  //           //   outgoingCallScreen: false,
  //           //   callInProgress: null,
  //           //   callSettings: null,
  //           // });
            
  //         },
  //         onError: (error:any) => {
  //           // logger('[OngoingCallListener] Call Error: ', error);
  //         },
  //       });
  
  //       const callSettings = new CometChat.CallSettingsBuilder()
  //         .setSessionID(sessionId)
  //         .enableDefaultLayout(true)
  //         .setIsAudioOnlyCall(audioOnly)
  //         .setCallEventListener(callListener)
  //         .build();
  
  //         setCallSettings(callSettings);
  //     } catch (error) {
  //       console.log(error);
  //     }
    
// }

  useEffect(() => {
    try {
      if (route?.params?.outGoingCall) {
        // playOutgoingAlert();

        const call = route?.params?.outGoingCall;

        setOutgoingCallScreen(true);
        setCallInProgress(call);
        setErrorScreen(false);
        setErrorMessage(null);
      }

    //   if (incomingCall) {
    //     acceptCall();
    //   }

      if (!route?.params?.outGoingCall) {
        setOutgoingCallScreen(false);
        setCallInProgress(null);
        setErrorScreen(false);
        setErrorMessage(null);
        setCallSettings(false)
      }
    } catch (error) {
    //   logger(error);
    }
  }, [route?.params?.outGoingCall]);


  /**
   * Handle rejecting call from outgoing call screen
   * @param
   */
 const cancelCall = () => {
    var sessionID = route?.params?.outGoingCall?.sessionId;
    var status = CometChat.CALL_STATUS.REJECTED;
    
    CometChat.rejectCall(sessionID, status).then(
      call => {
        console.log("Call rejected successfully", call);
        navigation.goBack()
      },
      error => {
        console.log("Call rejection failed with error:", error);
        navigation.goBack()
      }
    );
    
  };

  const callListener = new CometChatCalls.OngoingCallListener({
    onUserJoined: user => {
        console.log("user joined:", user);
    },
    onUserLeft: user => {
        console.log("user left:", user);
    },
    onUserListUpdated: userList => {
        console.log("user list:", userList);
    },
    onCallEnded: () => {
        console.log("Call ended");
    },
    onCallEndButtonPressed: () => {
        console.log("End Call button pressed");
    },
    onError: error => {
        console.log('Call Error: ', error);
    },
    onAudioModesUpdated: (audioModes) => {
        console.log("audio modes:", audioModes);
    },
    onCallSwitchedToVideo: (event) => {
        console.log("call switched to video:", event);
    },
    onUserMuted: (event) => {
        console.log("user muted:", event);
    }
});

  if (callSettings) {
    return (
      <Modal animated animationType="fade">
        <View style={{ height: '100%', width: '100%', position: 'relative' }}>
          {/* <KeepAwake /> */}
          <CometChatOngoingCall
              callSettingsBuilder={new CometChatCalls.CallSettingsBuilder()
                  .enableDefaultLayout(true)
                  .setCallEventListener(callListener)
            }
              sessionID={route?.params?.outGoingCall?.sessionId}
            />
        </View>
      </Modal>
    );
  }

  let callScreen = null;
  let errorScreenComponent = null;

  if (callInProgress) {
    if (errorScreen) {
      errorScreenComponent = (
        <View>
          <Text>{errorMessage}</Text>
        </View>
      );
    }

    if (outgoingCallScreen) {
      callScreen = (
        <Modal animated animationType="fade">
          <View style={style.container}>
            <View style={style.header}>
              <Text style={style.headerLabel}>Calling...</Text>
              <Text style={style.headerName}>{callInProgress.receiver.name}</Text>
            </View>
            <View style={style.thumbnail}>
              <CometChatAvatar
                cornerRadius={1000}
                borderColor={'yellow'}
                borderWidth={0}
                textFontSize={60}
                image={{ uri: callInProgress.receiver.avatar }}
                name={callInProgress.receiver.name}
              />
            </View>
            {errorScreenComponent}
            <View style={style.iconWrapper}>
              <TouchableOpacity onPress={()=>cancelCall()}>
                <View style={style.cancelBtn}>
                  <Icon name="call-end" color="#FFFFFF" size={32} />
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>
      );
    }
  }

  return callScreen;
};

export default CometChatOutgoingCall;

const style = StyleSheet.create({
    container: {
        flex: 1,
        textAlign: 'center',
        alignItems: 'center',
        flexDirection: 'column',
        padding: 20,
        justifyContent: 'space-between',
      },
      header: {
        padding: 20,
      },
      headerLabel: {
        fontSize: 13,
        textAlign: 'center',
      },
      headerName: {
        fontSize: 16,
        fontWeight: '700',
        textTransform: 'capitalize',
      },
      thumbnailWrapper: {
        width: 200,
      },
      avatarStyle: {
        flexWrap: 'wrap',
        flexDirection: 'row',
        width: 44,
        height: 44,
        borderRadius: 22,
        backgroundColor: 'rgba(51,153,255,0.25)',
        marginRight: 15 ,
      },
      thumbnail: {
        width: 200,
        flexWrap: 'wrap',
        flexDirection: 'row',
        height: 200,
        borderRadius: 200,
        backgroundColor: 'rgba(51,153,255,0.25)',
        marginRight: 15 ,
        overflow: 'hidden',
      },
      iconWrapper: {
        padding: 40 ,
      },
      cancelBtn: {
        width: 64,
        height: 64,
        margin: 12,
        padding: 16,
        borderRadius: 32,
        backgroundColor: '#FF3C2F',
      },
})
