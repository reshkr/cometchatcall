import React, { useState } from 'react';
import { get as _get } from 'lodash';
import { View, Image, Text, ImageSourcePropType, ViewStyle, TextStyle,StyleSheet} from 'react-native';

interface CometChatAvatarProps {
  borderWidth?: number;
  borderColor?: string;
  cornerRadius?: number;
  textFontSize?: number;
  textColor?: string;
  image?: ImageSourcePropType;
  name?: string;
}

const CometChatAvatar: React.FC<CometChatAvatarProps> = (props) => {
  const [borderWidth] = useState<number>(_get(props, 'borderWidth', 1));
  const [borderColor] = useState<string>(_get(props, 'borderColor', '#AAA'));
  const [cornerRadius] = useState<number>(_get(props, 'cornerRadius', 1000));
  const [textFontSize] = useState<number>(_get(props, 'textFontSize', 18));
  const [textColor] = useState<string>(_get(props, 'textColor', 'black'));

  const { image, name } = props;

  if (!(image && image.uri) && name) {
    return (
      <View
        style={[
          style.defaultAvatarContainer,
          {
            borderRadius: cornerRadius,
            borderWidth,
            borderColor,
          },
        ]}
      >
        <Text style={{ fontSize: textFontSize, color: textColor }}>
          {name[0].toUpperCase()}
        </Text>
      </View>
    );
  }

  return (
    <View
      style={[
        style.avatarContainer,
        {
          borderRadius: cornerRadius,
          borderWidth,
          borderColor,
        },
      ]}
    >
      <Image source={image as ImageSourcePropType} alt="CometChatAvatar" style={style.imageStyle} />
    </View>
  );
};

export default CometChatAvatar;

const style = StyleSheet.create({
    avatarContainer: {
        overflow: 'hidden',
        borderStyle: 'solid',
      },
      defaultAvatarContainer: {
        overflow: 'hidden',
        height: '100%',
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
      },
      imageStyle: {
        aspectRatio: 1,
        width: '100%',
        height: undefined,
      },
    
      groupDetailContainer: {
        padding: 16,
        flexDirection: 'row',
        alignItems: 'center',
      },
      groupDetail: {
        paddingLeft: 8,
        justifyContent: 'center',
        width: '100%',
      },
      userName: {
        fontSize: 18,
        color: 'red',
        fontWeight: 'bold',
      },
})