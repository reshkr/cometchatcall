import React, { useEffect, useState } from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  TextInput
} from 'react-native';
import { CometChat } from '@cometchat-pro/react-native-chat';
import AsyncStorage from "@react-native-async-storage/async-storage";
import { CometChatUIKit } from '@cometchat/chat-uikit-react-native'
import Api from '../../Api';

interface CometChatOutgoingCallProps {
    route:any;
    navigation:any;
}

const HomeScreen: React.FC<CometChatOutgoingCallProps> = ({route,navigation}) => {
    
    const [number, onChangeNumber] = React.useState('');

    useEffect(() => {
        navigateAlreadyLogin()
    }, [])

    const navigateAlreadyLogin = async () =>{
        try{
        const uids = await AsyncStorage.getItem('userAccessToken');  
        if(uids){
         navigation.navigate('HomeScreen')
        }
    } catch (error) { 
        console.error('Error subscribing to topics:', error);
      }
    }

    const userLoginSDK = (AUTH_TOKEN: string) => {
        CometChat.getLoggedinUser().then(
            user => {
                console.log("users",user)
                if (!user) {
                    console.log("users auth token",AUTH_TOKEN)
                    CometChat.login(AUTH_TOKEN).then(
                        user => {
                            navigation.navigate('HomeScreen',{user:user})
                            console.log("Login Successful:", { user });
                        }, error => {
                            
                            console.log("Login failed with exception:", { error });
                        }
                    );
                }
            }, error => {
                console.log("Something went wrong", error);
                
            }
        );
    }

    const phoneNumberValidation = () => {
                let payload = { country_code: '+91', mobile: number }
                console.log(payload)
                Api.sendOtp(payload).then((response) => {
                    let data = response.data
                    console.log(data)
                    if (data.status == 200) {
                        
                        console.log('OTP', data?.data?.user?.otp)
                        let payload = { country_code: '+91', mobile: number, otp: data?.data?.user?.otp , device_token:'kojiouiuyfhffgdtcgcfgd'}
                        console.log("verify otp",payload);
                        Api.verifyOtp(payload).then(async (response) => {
                            let data = response.data
                            console.log("success", data)
                            if (data.status == 200) {
                                    await AsyncStorage.setItem('userAccessToken', data?.data?.user?.access_token)
                                    await AsyncStorage.setItem('currentUserUid', data?.data?.user?.uuid)
                                    userLoginSDK(data?.data?.cometToken)
                            }
                        }).catch((error) => {
                            console.log('---',error.response)
                            if (error.response && error.response.status === 400) {
                                let data = error.response.data
                            }
                            else {
                                console.log(error.response)
                            }
                        });
                    }
                }).catch((error) => {
                    console.log('---',error.response)
                    if (error.response && error.response.status === 400) {
                        let data = error.response.data
                    }
                    else {
                       
                    }
                });
          
    };
  

    return (
     
        <View style={{ height: '100%', width: '100%', position: 'relative' ,alignItems:'center',justifyContent:'center'}}>

                <TextInput
                    style={{height: 40,
                        margin: 12,
                        borderWidth: 1,
                        width:'90%',
                        padding: 10,}}
                    onChangeText={onChangeNumber}
                    value={number}
                    placeholder="Enter Phone Number"
                    keyboardType="numeric"
                />
            <TouchableOpacity 
            style={{width:100,height:50,backgroundColor:'blue',alignItems:'center',justifyContent:'center'}}
            onPress={()=>phoneNumberValidation()}>
                <Text style={{fontSize:20,color:'white'}}>Login</Text>
            </TouchableOpacity>
            
         
        </View>
     
    );
 
}
export default HomeScreen;
