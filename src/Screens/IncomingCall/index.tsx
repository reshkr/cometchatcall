import React, { useEffect, useState } from 'react';
import {
  Text,
  View,
  Modal,
  TouchableOpacity,
  Vibration,
  StyleSheet,
  SafeAreaView,
  Image
} from 'react-native';
import { CometChat } from '@cometchat-pro/react-native-chat';
import Icon from 'react-native-vector-icons/MaterialIcons';
// import Sound from 'react-native-sound';
import CometChatAvatar from '../CallScreen/CometChatAvatar';
import  {CometChatCalls}  from "@cometchat-pro/react-native-calls";
import {CometChatOngoingCall} from '@cometchat/chat-uikit-react-native'

import audioCallIcon from './resources/incomingaudiocall.png';
import videoCallIcon from './resources/incomingvideocall.png';

interface CometChatOutgoingCallProps {
    route:any;
    navigation:any;
    callback: (event: string, call: any) => void;
}

const CometChatOutgoingCall: React.FC<CometChatOutgoingCallProps> = ({route,navigation}) => {
  const [callSettings, setCallSettings] = useState<any>(null);
  const [errorScreen, setErrorScreen] = useState<boolean>(false);
  const [errorMessage, setErrorMessage] = useState<string | null>(null);
  const [callInProgress, setCallInProgress] = useState<any>(null);
  const [outgoingCallScreen, setOutgoingCallScreen] = useState<boolean>(false);

  const startCall = (call:any) => {
    try {
          const { sessionId } = call;
          const callType = call.type;
          const audioOnly = callType === 'audio';

          const callListener = new CometChatCalls.OngoingCallListener({
            onUserJoined: user => {
                console.log("user joined:", user);
            },
            onUserLeft: user => {
                console.log("user left:", user);
            },
            onUserListUpdated: userList => {
                console.log("user list:", userList);
            },
            onCallEnded: () => {
                console.log("Call ended");
            },
            onCallEndButtonPressed: () => {
                console.log("End Call button pressed");
            },
            onError: error => {
                console.log('Call Error: ', error);
            },
            onAudioModesUpdated: (audioModes) => {
                console.log("audio modes:", audioModes);
            },
            onCallSwitchedToVideo: (event) => {
                console.log("call switched to video:", event);
            },
            onUserMuted: (event) => {
                console.log("user muted:", event);
            }
        });
        
        const callSettings = new CometChat.CallSettingsBuilder()
        .setSessionID(sessionId)
        .enableDefaultLayout(true)
        .setIsAudioOnlyCall(audioOnly)
        .setCallEventListener(callListener)
        .build();
        setCallSettings(callSettings);
      } catch (error) {
        console.log(error);
      }
    
}

useEffect(() => {
  try {
    if (route?.params?.outGoingCall) {
      // playOutgoingAlert();

      const call = route?.params?.outGoingCall;

      setOutgoingCallScreen(true);
      setCallInProgress(call);
      setErrorScreen(false);
      setErrorMessage(null);
    }

  //   if (incomingCall) {
  //     acceptCall();
  //   }

    if (!route?.params?.outGoingCall) {
      setOutgoingCallScreen(false);
      setCallInProgress(null);
      setErrorScreen(false);
      setErrorMessage(null);
      setCallSettings(false)
    }
  } catch (error) {
  //   logger(error);
  }
}, [route?.params?.outGoingCall]);



  /**
   * Handle rejecting call from outgoing call screen
   * @param
   */
 const acceptCall = () => {
    var sessionID = route?.params?.outGoingCall?.sessionId;
    CometChat.acceptCall(sessionID).then(
      call => {
        console.log("Call accepted successfully", call);
        // startCall(call)
        // navigation.goBack()
        setCallSettings(true)
      },
      error => {
        console.log("Call rejection failed with error:", error);
        navigation.goBack()
      }
    );
    
  };

  const rejectCall = () => {
    var sessionID = route?.params?.outGoingCall?.sessionId;
    var status = CometChat.CALL_STATUS.REJECTED;
    
    CometChat.rejectCall(sessionID,status).then(
      call => {
        console.log("Call rejected successfully", call);
        // startCall(call)
        // navigation.goBack()
      },
      error => {
        console.log("Call rejection failed with error:", error);
        navigation.goBack()
      }
    );
    
  };

  const callListener = new CometChatCalls.OngoingCallListener({
    onUserJoined: user => {
        console.log("user joined:", user);
    },
    onUserLeft: user => {
        console.log("user left:", user);
    },
    onUserListUpdated: userList => {
        console.log("user list:", userList);
    },
    onCallEnded: () => {
        console.log("Call ended");
    },
    onCallEndButtonPressed: () => {
        console.log("End Call button pressed");
    },
    onError: error => {
        console.log('Call Error: ', error);
    },
    onAudioModesUpdated: (audioModes) => {
        console.log("audio modes:", audioModes);
    },
    onCallSwitchedToVideo: (event) => {
        console.log("call switched to video:", event);
    },
    onUserMuted: (event) => {
        console.log("user muted:", event);
    }
});

if (callSettings) {
    return (
      <Modal animated animationType="fade">
        <View style={{ height: '100%', width: '100%', position: 'relative' }}>
          {/* <KeepAwake /> */}
          <CometChatOngoingCall
              callSettingsBuilder={new CometChatCalls.CallSettingsBuilder()
                  .enableDefaultLayout(true)
                  .setCallEventListener(callListener)
            }
              sessionID={route?.params?.outGoingCall?.sessionId}
            />
        </View>
      </Modal>
    );
  }

  let callScreen = null;
  let errorScreenComponent = null;

  if (callInProgress) {
    if (errorScreen) {
      errorScreenComponent = (
        <View>
          <Text>{errorMessage}</Text>
        </View>
      );
    }

    if (outgoingCallScreen) {
      callScreen = (
        <Modal  transparent animated animationType="fade">
        <SafeAreaView>
          <View style={[style.callContainerStyle]}>
            <View style={style.senderDetailsContainer}>
              <View>
                <Text numberOfLines={1} style={style.nameStyle}>
                  {callInProgress.sender.name}
                </Text>
                {callInProgress.type === 'video' ? (
                  <View style={style.callTypeStyle}>
                    <Image source={videoCallIcon} alt="Incoming video call" />
                    <View style={style.callMessageContainerStyle}>
                      <Text
                        numberOfLines={1}
                        style={style.callMessageTextStyle}>
                        Incoming video call
                      </Text>
                    </View>
                  </View>
                ) : (
                  <View style={style.callTypeStyle}>
                    <Image source={audioCallIcon} alt="Incoming Audio call" />
                    <View style={style.callMessageContainerStyle}>
                      <Text
                        numberOfLines={1}
                        style={style.callMessageTextStyle}>
                        Incoming audio call
                      </Text>
                    </View>
                  </View>
                )}
              </View>
              <View style={style.avatarStyle}>
                <CometChatAvatar
                  cornerRadius={1000}
                  borderWidth={0}
                  textColor="white"
                  image={{ uri: callInProgress.sender.avatar }}
                  name={callInProgress.sender.name}
                />
              </View>
            </View>
            <View style={style.headerButtonStyle}>
              <TouchableOpacity
                style={[
                  style.buttonStyle,
                  { backgroundColor: 'red' },
                ]}
                onPress={rejectCall}>
                <Text style={style.btnTextStyle}>Decline</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={[
                  style.buttonStyle,
                  { backgroundColor: 'blue' },
                ]}
                onPress={acceptCall}>
                <Text style={style.btnTextStyle}>Accept</Text>
              </TouchableOpacity>
            </View>
          </View>
        </SafeAreaView>
      </Modal>
      );
    }
  }

  return callScreen;
};

export default CometChatOutgoingCall;

const style = StyleSheet.create({
  callContainerStyle: {
    marginTop: 20,
    marginHorizontal: 40 ,
    borderRadius: 15,
    padding: 20 ,
    backgroundColor: '#444444',
  },
  senderDetailsContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  callMessageContainerStyle: { marginLeft: 5 },
  callMessageTextStyle: { color: '#8A8A8A' },
  nameStyle: {
    marginBottom: 4 ,
    color: 'white',
    fontSize: 15,
    width: 150 ,
    fontWeight: '900',
  },
  btnTextStyle: { color: 'white', textAlign: 'center' },
  avatarStyle: {
    flexWrap: 'wrap',
    width: 34,
    height: 34,
    borderRadius: 22,
    backgroundColor: 'rgba(51,153,255,0.25)',
  },
  callTypeStyle: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  headerButtonStyle: {
    marginTop: 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  buttonStyle: {
    paddingVertical: 6 ,
    borderRadius: 5,
    width: '45%',
  },
})
