import React,{useEffect,useState} from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

// ------------- Screens Import Start-----------------------
import LoginScreen from '../Screens/Loginscreen';
import HomeScreen from '../Screens/HomeScreen';
import CallScreen from '../Screens/CallScreen';
import IncomingCall from '../Screens/IncomingCall'
// ------------- Screens Import End-----------------------

export type RootStackParamList = {
    LoginScreen:any;
    HomeScreen:any;
    CallScreen:any;
    IncomingCall:any
};

const Stack = createNativeStackNavigator<RootStackParamList>();

const StackNavigator: React.FC = () => {
    const navigationRef = React.useRef();

  


    return (
        <NavigationContainer
            ref={navigationRef}
        >
            <Stack.Navigator screenOptions={{
                headerShown: false
            }}
            >
                <Stack.Screen name="LoginScreen" component={LoginScreen} />
                <Stack.Screen name="HomeScreen" component={HomeScreen} />
                <Stack.Screen name="CallScreen" component={CallScreen} />
                <Stack.Screen name="IncomingCall" component={IncomingCall} />
            </Stack.Navigator>
        </NavigationContainer>
    );
}

export default StackNavigator;