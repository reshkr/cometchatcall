
import AsyncStorage from "@react-native-async-storage/async-storage";
import axios1 from "axios";

export const constants = {
  baseURL: "https://bbt.iroidtechnologies.in/api/",      //developement
  cometApiKey : "5cd9c0ee2e2df707ce7e925233c70532dde79e34",// remove this after removing cometchat api api 
  cometBaseURL : "https://245904647869399c.api-in.cometchat.io/v3/",
  AppId : '245904647869399c',
  cometChatAuthKey : '8c7395fbf05e35645301ffca7418c1df596ab3e9',
  // baseURL: "https://bbt.iroidtechnologies.in/api/"       //production
};
export const axios = axios1.create({
  baseURL: constants.baseURL,
  timeout: 30000,
  headers: { "Content-Type": "application/json",
  Accept: 'application/json',
  'Access-Control-Allow-Origin': '*', },
});
export const multiPartAxios = axios1.create({
  baseURL:constants.baseURL,
  timeout: 30000,
  headers: { "Content-Type": "multipart/form-data",
  Accept: 'multipart/form-data',
  'Access-Control-Allow-Origin': '*', },
});

axios.interceptors.request.use(async function (_config) {
  var accessToken = await AsyncStorage.getItem('userAccessToken');
  console.log("access token in config >>",accessToken)
  if (accessToken !== null) {
    _config.headers.Authorization = 'Bearer ' + accessToken;
  }
  return _config;
});

multiPartAxios.interceptors.request.use(async function (_config) {
  var accessToken = await AsyncStorage.getItem('userAccessToken');
  // console.log("access token in config >>",accessToken)
  if (accessToken !== null) {
    _config.headers.Authorization = 'Bearer ' + accessToken;
  }
  return _config;
});
multiPartAxios.defaults.timeout = 30000;
axios.defaults.timeout = 30000;

export const axiosCometChat = axios1.create({
  baseURL: constants.cometBaseURL,
  timeout: 30000,
  headers: {
    "Content-Type": "application/json",
    'Accept': 'application/json',
    'Access-Control-Allow-Origin': '*',
    'Apikey': constants.cometApiKey
  },
});

axiosCometChat.interceptors.request.use(async function (_config) {
  var currentUserUid = await AsyncStorage.getItem('currentUserUid');
  // console.log("access token in config >>",currentUserUid)
    console.log("interceptor config >>")
  // _config.headers.onBehalfOf = '1aa9e89a-6e5d-4020-88ba-9be86e436ac2'
  if (currentUserUid !== null) {
    _config.headers.onBehalfOf = currentUserUid;
  }
  return _config;
});

axiosCometChat.defaults.timeout = 30000
