import React, { useEffect } from 'react'
import 'react-native-reanimated'
import {
  LogBox,
  PermissionsAndroid,
  Platform
} from 'react-native';

// ---------------------- Comechat Imports --------------------------------------
import { CometChat } from "@cometchat-pro/react-native-chat";
import { CometChatCalls } from "@cometchat-pro/react-native-calls";
import { CometChatUIKit, UIKitSettings } from '@cometchat/chat-uikit-react-native'

// ---------------------------- Navigation Imports ------------------------------
import Navigation from './src/Navigations'

function App(): JSX.Element {
 
  useEffect(() => {
    initializeCometChat()
    initializeCometChatCall()
  }, [])

  const initializeCometChat = (() => {
    var appSetting = new CometChat.AppSettingsBuilder().subscribePresenceForAllUsers().setRegion('in').autoEstablishSocketConnection(true).build();
    CometChat.init('245904647869399c', appSetting).then(
      () => {
        console.log("CometChat Initialization completed successfully");
      },
      error => {
        console.log("CometChat Initialization failed with error:", error);
      }
    );
  })

  
  const initializeCometChatCall = (() => {
    const callAppSettings = new CometChatCalls.CallAppSettingsBuilder()
      .setAppId('245904647869399c')
      .setRegion('in')
      .build();
    CometChatCalls.init(callAppSettings).then(
      () => {
        console.log('CometChatCalls initialization completed successfully');
      },
      error => {
        console.log('CometChatCalls initialization failed with error:', error);
      },
    );
  })

  LogBox.ignoreAllLogs();

  const getPermissions = () => {
    if (Platform.OS == "android") {
      PermissionsAndroid.requestMultiple([
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
        PermissionsAndroid.PERMISSIONS.CAMERA,
        PermissionsAndroid.PERMISSIONS.RECORD_AUDIO,
      ]);
    }
}

  useEffect(() => {
    getPermissions();
    let uikitSettings : UIKitSettings= {
      appId: '245904647869399c',
      authKey: '8c7395fbf05e35645301ffca7418c1df596ab3e9',
      region: 'in',
    };
    CometChatUIKit.init(uikitSettings)
    .then(() => {
        console.log("CometChatUiKit successfully initialized")
    })
    .catch((error) => {
        console.log("Initialization failed with exception:", error)
    })
  },[]);

  return (
          <Navigation/>
  );
}

export default App;
